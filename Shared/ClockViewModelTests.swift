//
//  ClockViewModelTests.swift
//  BerlinClock
//
//  Created by 2021-DEV2-018 on 16/04/2021.
//

@testable import BerlinClock
import XCTest

class ClockViewModelTests: XCTestCase {

    var sut: ClockViewModel!

    override func setUpWithError() throws {
        try super.setUpWithError()

        var calendar = Calendar(identifier: .gregorian)
        calendar.locale = Locale(identifier: "nl_BE")
        calendar.timeZone = TimeZone(identifier: "GMT")!
        sut = ClockViewModel(calendar: calendar)
    }

    override func tearDownWithError() throws {
        try super.tearDownWithError()

        sut = nil
    }

    func testInit() throws {
        let sut = ClockViewModel()
        XCTAssertEqual(sut.clockState, .empty)
    }

    func testSeconds() throws {
        sut.updateState(for: Date(timeIntervalSince1970: 1))

        XCTAssertEqual(sut.clockState.seconds, .off)

        sut.updateState(for: Date(timeIntervalSince1970: 2))

        XCTAssertEqual(sut.clockState.seconds, .red)

        sut.updateState(for: Date(timeIntervalSince1970: 640265251)) // 11:27

        XCTAssertEqual(sut.clockState.seconds, .off)
    }

    func testMinutes() throws {
        sut.updateState(for: Date(timeIntervalSince1970: 1))

        XCTAssertEqual(sut.clockState.minutesRow1, Array(repeating: .off, count: 11))
        XCTAssertEqual(sut.clockState.minutesRow2, [.off, .off, .off, .off])

        sut.updateState(for: Date(timeIntervalSince1970: 2))

        XCTAssertEqual(sut.clockState.minutesRow1, Array(repeating: .off, count: 11))
        XCTAssertEqual(sut.clockState.minutesRow2, [.off, .off, .off, .off])

        sut.updateState(for: Date(timeIntervalSince1970: 640265251)) // 11:27

        XCTAssertEqual(sut.clockState.minutesRow1, [.yellow, .yellow, .red, .yellow, .yellow, .off, .off, .off, .off, .off, .off])
        XCTAssertEqual(sut.clockState.minutesRow2, [.yellow, .yellow, .off, .off])
    }

    func testHours() throws {
        sut.updateState(for: Date(timeIntervalSince1970: 1))

        XCTAssertEqual(sut.clockState.hoursRow1, [.off, .off, .off, .off])
        XCTAssertEqual(sut.clockState.hoursRow2, [.off, .off, .off, .off])

        sut.updateState(for: Date(timeIntervalSince1970: 2))

        XCTAssertEqual(sut.clockState.hoursRow1, [.off, .off, .off, .off])
        XCTAssertEqual(sut.clockState.hoursRow2, [.off, .off, .off, .off])

        sut.updateState(for: Date(timeIntervalSince1970: 640265251)) // 11:27

        XCTAssertEqual(sut.clockState.hoursRow1, [.red, .red, .off, .off])
        XCTAssertEqual(sut.clockState.hoursRow2, [.red, .off, .off, .off])
    }

    func testTime() throws {
        sut.updateState(for: Date(timeIntervalSince1970: 1))

        XCTAssertEqual(sut.clockState.time, "00:00")

        sut.updateState(for: Date(timeIntervalSince1970: 2))

        XCTAssertEqual(sut.clockState.time, "00:00")

        sut.updateState(for: Date(timeIntervalSince1970: 640265251)) // 11:27

        XCTAssertEqual(sut.clockState.time, "11:27")
    }
}
