//
//  ClockViewModel.swift
//  BerlinClock
//
//  Created by 2021-DEV2-018 on 16/04/2021.
//

import Combine
import Foundation

class ClockViewModel: ObservableObject {
    @Published var clockState: ClockState = .empty

    private var subscriptions = Set<AnyCancellable>()
    private let calendar: Calendar
    private lazy var timeFormatter: DateFormatter = {
        let df = DateFormatter()
        df.calendar = self.calendar
        df.timeZone = self.calendar.timeZone
        df.locale = self.calendar.locale
        df.timeStyle = .short
        df.dateStyle = .none
        return df
    }()

    init(calendar: Calendar = .autoupdatingCurrent) {
        self.calendar = calendar

        Timer.publish(every: 1, on: .main, in: .common)
            .autoconnect()
            .sink { (_) in
                self.updateState(for: Date())
            }
            .store(in: &subscriptions)
    }

    func updateState(for date: Date) {
        clockState = ClockState(date: date, calendar: calendar, timeFormatter: timeFormatter)
    }
}
