//
//  ContentView.swift
//  Shared
//
//  Created by 2021-DEV2-018 on 15/04/2021.
//

import Combine
import SwiftUI

struct ContentView: View {
    @StateObject var viewModel = ClockViewModel()

    var body: some View {
        VStack {
            // Seconds
            Circle()
                .strokeBorder(lineWidth: 4)
                .foregroundColor(.gray)
                .background(viewModel.clockState.seconds.color)
                .clipShape(Circle())

            // Hours, row 1 (per 5h)
            HStack {
                ForEach(viewModel.clockState.hoursRow1, id: \.self) { lamp in
                    Rectangle()
                        .strokeBorder(lineWidth: 4)
                        .foregroundColor(.gray)
                        .background(lamp.color)
                        .clipShape(Rectangle())
                }
            }

            // Hours, row 2 (per 1h)
            HStack {
                ForEach(viewModel.clockState.hoursRow2, id: \.self) { lamp in
                    Rectangle()
                        .strokeBorder(lineWidth: 4)
                        .foregroundColor(.gray)
                        .background(lamp.color)
                        .clipShape(Rectangle())
                }
            }

            // Minutes, row 1 (per 5min)
            HStack {
                ForEach(viewModel.clockState.minutesRow1, id: \.self) { lamp in
                    Rectangle()
                        .strokeBorder(lineWidth: 4)
                        .foregroundColor(.gray)
                        .background(lamp.color)
                        .clipShape(Rectangle())
                }
            }
            // Minutes, row 2 (per 1min)
            HStack {
                ForEach(viewModel.clockState.minutesRow2, id: \.self) { lamp in
                    Rectangle()
                        .strokeBorder(lineWidth: 4)
                        .foregroundColor(.gray)
                        .background(lamp.color)
                        .clipShape(Rectangle())
                }
            }

            Text(viewModel.clockState.time)
                .font(.largeTitle)
        }
        .padding()
    }
}

extension LampState {
    var color: Color {
        switch self {
        case .off: return .clear
        case .red: return .red
        case .yellow: return .yellow
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
