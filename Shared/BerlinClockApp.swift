//
//  BerlinClockApp.swift
//  Shared
//
//  Created by 2021-DEV2-018 on 15/04/2021.
//

import SwiftUI

@main
struct BerlinClockApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
