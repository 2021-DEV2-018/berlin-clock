//
//  ClockStateTests.swift
//  BerlinClock
//
//  Created by Tim Viaene on 16/04/2021.
//

@testable import BerlinClock
import XCTest

class ClockStateTests: XCTestCase {

    func testSeconds() {
        XCTAssertEqual(ClockState.lampStateSeconds(for: 0), .red)
        XCTAssertEqual(ClockState.lampStateSeconds(for: 1), .off)
        XCTAssertEqual(ClockState.lampStateSeconds(for: 2), .red)
        XCTAssertEqual(ClockState.lampStateSeconds(for: 19), .off)
    }

    func testMinutes() {
        XCTAssertEqual(ClockState.lampStateMinutesRow1(for: 0), Array(repeating: .off, count: 11))
        XCTAssertEqual(ClockState.lampStateMinutesRow2(for: 0), Array(repeating: .off, count: 4))

        XCTAssertEqual(ClockState.lampStateMinutesRow1(for: 1), Array(repeating: .off, count: 11))
        XCTAssertEqual(ClockState.lampStateMinutesRow2(for: 1), [.yellow, .off, .off, .off])

        XCTAssertEqual(ClockState.lampStateMinutesRow1(for: 2), Array(repeating: .off, count: 11))
        XCTAssertEqual(ClockState.lampStateMinutesRow2(for: 2), [.yellow, .yellow, .off, .off])

        XCTAssertEqual(ClockState.lampStateMinutesRow1(for: 7),
                       [.yellow, .off, .off, .off, .off, .off, .off, .off, .off, .off, .off])
        XCTAssertEqual(ClockState.lampStateMinutesRow2(for: 7), [.yellow, .yellow, .off, .off])

        XCTAssertEqual(ClockState.lampStateMinutesRow1(for: 55),
                       [.yellow, .yellow, .red, .yellow, .yellow, .red, .yellow, .yellow, .red, .yellow, .yellow])
        XCTAssertEqual(ClockState.lampStateMinutesRow2(for: 55), [.off, .off, .off, .off])
    }

    func testHours() {
        XCTAssertEqual(ClockState.lampStateHoursRow1(for: 0), [.off, .off, .off, .off])
        XCTAssertEqual(ClockState.lampStateHoursRow2(for: 0), [.off, .off, .off, .off])

        XCTAssertEqual(ClockState.lampStateHoursRow1(for: 1), [.off, .off, .off, .off])
        XCTAssertEqual(ClockState.lampStateHoursRow2(for: 1), [.red, .off, .off, .off])

        XCTAssertEqual(ClockState.lampStateHoursRow1(for: 5), [.red, .off, .off, .off])
        XCTAssertEqual(ClockState.lampStateHoursRow2(for: 5), [.off, .off, .off, .off])

        XCTAssertEqual(ClockState.lampStateHoursRow1(for: 13), [.red, .red, .off, .off])
        XCTAssertEqual(ClockState.lampStateHoursRow2(for: 13), [.red, .red, .red, .off])

        XCTAssertEqual(ClockState.lampStateHoursRow1(for: 24), [.red, .red, .red, .red])
        XCTAssertEqual(ClockState.lampStateHoursRow2(for: 24), [.red, .red, .red, .red])
    }
}
