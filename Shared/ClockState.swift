//
//  ClockState.swift
//  BerlinClock
//
//  Created by Tim Viaene on 16/04/2021.
//

import Foundation

enum LampState: Equatable {
    case red, yellow, off
}

struct ClockState: Equatable {
    var time: String
    var seconds: LampState
    var minutesRow1: [LampState]
    var minutesRow2: [LampState]
    var hoursRow1: [LampState]
    var hoursRow2: [LampState]

    static var empty: Self = ClockState(time: "",
                                        seconds: .off,
                                        minutesRow1: Array(repeating: .off, count: 11),
                                        minutesRow2: Array(repeating: .off, count: 4),
                                        hoursRow1: Array(repeating: .off, count: 4),
                                        hoursRow2: Array(repeating: .off, count: 4))
}

extension ClockState {
    init(date: Date, calendar: Calendar, timeFormatter: DateFormatter) {
        time = timeFormatter.string(from: date)

        let timeComponents = calendar.dateComponents([.hour, .minute, .second], from: date)

        seconds = ClockState.lampStateSeconds(for: timeComponents.second ?? 0)

        let minutes = timeComponents.minute ?? 0
        minutesRow1 = ClockState.lampStateMinutesRow1(for: minutes)
        minutesRow2 = ClockState.lampStateMinutesRow2(for: minutes)

        let hours = timeComponents.hour ?? 0
        hoursRow1 = ClockState.lampStateHoursRow1(for: hours)
        hoursRow2 = ClockState.lampStateHoursRow2(for: hours)
    }

    static func lampStateSeconds(for seconds: Int) -> LampState {
        seconds % 2 == 0 ? .red : .off
    }

    static func lampStateMinutesRow1(for minutes: Int) -> [LampState] {
        let lampsOn = Int(minutes / 5)
        let allYellowLamps: [LampState] = Array(repeating: .yellow, count: lampsOn) + Array(repeating: .off, count: 11 - lampsOn)

        return allYellowLamps.enumerated().map { (index, element) -> LampState in
            if index % 3 == 2 && element == .yellow {
                return .red
            } else {
                return element
            }
        }
    }

    static func lampStateMinutesRow2(for minutes: Int) -> [LampState] {
        let lampsOn = minutes % 5
        return Array(repeating: .yellow, count: lampsOn) + Array(repeating: .off, count: 4 - lampsOn)
    }

    static func lampStateHoursRow1(for hours: Int) -> [LampState] {
        let lampsOn = Int(hours / 5)
        return Array(repeating: .red, count: lampsOn) + Array(repeating: .off, count: 4 - lampsOn)
    }

    static func lampStateHoursRow2(for hours: Int) -> [LampState] {
        let lampsOn = hours % 5
        return  Array(repeating: .red, count: lampsOn) + Array(repeating: .off, count: 4 - lampsOn)
    }
}
