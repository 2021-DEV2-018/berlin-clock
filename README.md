# Berlin Clock Kata

Berlin Clock Kata implemented according to [these specifications](https://github.com/stephane-genicot/katas/blob/master/BerlinClock.md).

The app runs on macOS and iOS (iPod/iPhone/iPad). No special modifications have been made for any specific platform.

![macOS screenshot](screenshot-mac.png)
![iOS screenshot](screenshot-ios.png)

## Run the App

* Open BerlinClock.xcodeproj in Xcode (tested in Xcode 12.2 and 12.4)
* Select macOS or iOS target
* Hit "Product > Run"

## Run the Tests

* Open BerlinClock.xcodeproj in Xcode (tested in Xcode 12.2 and 12.4)
* Select macOS or iOS target
* Hit "Product > Test"

## Possible improvement

On macOS you can see that the seconds in the status bar clock can tick at a different time than the top lamp in the UI, depending on the instant the app was launched.
This could be fixed by computing the time until the next full second, and delaying the Timer publisher for that amount of time, but I have considered this improvement out of scope for this exercise.
